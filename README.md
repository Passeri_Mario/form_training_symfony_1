# form_training_symfony_1

Projet d’entrainement aux formulaires de symfony pour de la reservation

https://sully-training.mariopasseri.eu/

# Procédure utilisé 

** Publier application sur web **

1) Supprimer le cache dans le répertoire var/cache/dev

2) dans le fichier.env modifier ligne 17 en "APP_ENV=prod"

3) Supprimer var/cache/prod

4) Chercher "symfony/apache-pack" avec la commande : composer require symfony/apache-pack

5) Mettre infos de la DB dans .env

6) Copier avec le SFTP du répertoire (sauf .git et .idea)

7) Faire pointer le nom de domaine sur dossier public du projet




