//  Code pour les navigateurs qui ne dispose pas de datepicker //

let datefield = document.getElementById("date");

if (datefield.type!=="date"){
    document.write('<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css"/>\n');
    document.write('<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"><\/script>\n');

    $(document).ready(function() {
        document.getElementById('date').value = new Date()
            .toJSON()
            .substring(0,10)
            .replace("-", "/")
            .replace("-","/");

        // Desactivation des weekends
        $("#date").datepicker({
            beforeShowDay: $.datepicker.noWeekends,
            minDate: +0,
            dateFormat: 'dd/mm/yy'
        });
    });
}

// end //

function On_date(){
    // Ajout de la date du jour à l'input date de email
    document.getElementById("date").valueAsDate= new Date();
}

function VerifDate() {

    // Format date du jour

    let init_date_du_jour = new Date();

    let a = init_date_du_jour.getFullYear();
    let b= init_date_du_jour.getMonth()+1;
    let c = init_date_du_jour.getDate();

    // Détection des 9 premiers jours du mois

    if (init_date_du_jour.getDate() > 9 && init_date_du_jour.getMonth()+1 > 9) {
        var la_date_du_jour = a + "-" + b + "-" + c;
    }
    else {
        var la_date_du_jour = a + "-" + 0 + b + "-" + 0 + c;
    }

    // Récupération de la date du formulaire

    let input_date = document.getElementById('date').value // Le pseudo

    // Convertir au format date

    let date = new Date(input_date);

    if (datefield.type!=="date"){}
    // Si la date est un samedi ou un dimanche
    else {
        if(date.getDay() === 6 || date.getDay() === 0){

            // Gestion du input

            document.getElementById('date').style.border="1px solid red";
            document.getElementById('date').value="";

            //Gestion de la modal d'erreur

            document.getElementById('error-date').style.position="absolute";
            document.getElementById('error-date').style.transform="translateX(-50%)";
            document.getElementById('error-date').style.left="21%";
            document.getElementById('error-date').style.marginTop="34px"
            document.getElementById('error-date').style.display="block";
            document.getElementById('error-date').style.border="3px solid red";
            document.getElementById('error-date').style.fontSize="1.1rem";
            document.getElementById('error-date').style.textAlign="center";
            document.getElementById('error-date').style.backgroundColor="#313445";
            document.getElementById('error-date').style.color="white";
            document.getElementById('error-date').style.padding="17px";
            document.getElementById('error-date').style.width="310px";
            document.getElementById('error-date').textContent="Reservation du Lundi au Vendredi";
        }

        // Si la date retourne un jour de la semaine du lundi au vendredi

        else {
            document.getElementById('date').style.border="1px solid #b6b6b3";
            document.getElementById('date').style.borderRadius="5px";
            document.getElementById('error-date').style.display="none";
        }

        // Aucune réservation antérieure possible


        if (la_date_du_jour > input_date){

            // Gestion du input

            document.getElementById('date').style.border="1px solid red";
            document.getElementById('date').value="";

            // Gestion de la modal d'erreur

            document.getElementById('error-date').style.position="absolute";
            document.getElementById('error-date').style.transform="translateX(-50%)";
            document.getElementById('error-date').style.left="21%";
            document.getElementById('error-date').style.marginTop="34px"
            document.getElementById('error-date').style.display="block";
            document.getElementById('error-date').style.border="3px solid red";
            document.getElementById('error-date').style.fontSize="1.1rem";
            document.getElementById('error-date').style.textAlign="center";
            document.getElementById('error-date').style.backgroundColor="#313445";
            document.getElementById('error-date').style.color="white";
            document.getElementById('error-date').style.padding="17px";
            document.getElementById('error-date').style.width="310px";
            document.getElementById('error-date').textContent="Date déjà passée";
        }
    }
}

// Enregistrement des informations des champs du formulaire

if(typeof sessionStorage!='undefined') {

    // Listes des champs disponible sur le formulaire

    if('nom' in sessionStorage) {
        document.getElementById('nom').value = sessionStorage.getItem('nom');
    }

    if('prenom' in sessionStorage) {
        document.getElementById('prenom').value = sessionStorage.getItem('prenom');
    }

    if('telephone' in sessionStorage) {
        document.getElementById('telephone').value = sessionStorage.getItem('telephone');
    }

    if('courriel' in sessionStorage) {
        document.getElementById('courriel').value = sessionStorage.getItem('courriel');
    }

    if('heure' in sessionStorage) {
        document.getElementById('heure').value = sessionStorage.getItem('heure');
    }

    if('tarif' in sessionStorage) {
        document.getElementById('tarif').value = sessionStorage.getItem('tarif');
    }

    if('autorisation' in sessionStorage) {
        document.getElementById('autorisation').value = sessionStorage.getItem('autorisation');
    }

}

else {
    alert("Fonctionnalité sauvegarde des données temporaire non disponible");
    console.log("Fonctionnalité sauvegarde des données temporaire non disponible");
}

// Fonction pour les input qui oblige uniquement des chiffres

function Verif_nombre(champ) {

    let chiffres = new RegExp("[0-9]");
    let verif_nombre;
    let points = 0;

    for(let x = 0; x < champ.value.length; x++) {
        verif_nombre = chiffres.test(champ.value.charAt(x));
        if(champ.value.charAt(x) === "."){points++;}
        if(points > 1){verif_nombre = false; points = 1;}
        if(verif_nombre === false){champ.value = champ.value.substr(0,x) + champ.value.substr(x+1,champ.value.length-x+1); x--;}
    }
}

// Fonction pour les input qui oblige uniquement des lettres Majuscule Minuscule

function Verif_text(champ) {
    let texte = new RegExp("[A-Z a-z]");
    let verif_texte;
    let points = 0;

    for(let x = 0; x < champ.value.length; x++) {
        verif_texte = texte.test(champ.value.charAt(x));
        if(champ.value.charAt(x) === "."){points++;}
        if(points > 1){verif_texte = false; points = 1;}
        if(verif_texte === false){champ.value = champ.value.substr(0,x) + champ.value.substr(x+1,champ.value.length-x+1); x--;}
    }
}

// Fonction pour l'email qui oblige un format strict

function Verif_email(champ) {
    let email = new RegExp("[A-Z a-z.@0-9]");
    let verif_email;
    let points = 0;

    for(let x = 0; x < champ.value.length; x++) {
        verif_email= email.test(champ.value.charAt(x));
        if(champ.value.charAt(x) === "."){points++;}
        if(points > 1){verif_email = false; points = 1;}
        if(verif_email === false){champ.value = champ.value.substr(0,x) + champ.value.substr(x+1,champ.value.length-x+1); x--;}
    }
}
