<?php

namespace App\Form;

use App\Entity\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => "Nom"
            ])

            ->add('surname', TextType::class, [
                'label' => "Prenom"
            ])

            ->add('phone', TelType::class, [
                'label' => 'Téléphone'
            ])

            ->add('mail',EmailType::class, [
                'label' => 'Mail'
            ])
            ->add('date_rdv', DateType::class,[
                'label' => 'Date RDV',
                'widget' => 'single_text',
                ])
            ->add('time', ChoiceType::class, [
                'label' => 'Heure RDV',
                'choices'  => [
                    '09h00' => '09h00',
                    '10h00' => '10h00',
                    '11h00' => '11h00',
                    '12h00' => '12h00',
                    '13h00' => '13h00',
                    '14h00' => '14h00',
                    '15h00' => '15h00',
                    '16h00' => '16h00',
                    '17h00' => '17h00',
                ],
            ])
            ->add('price', ChoiceType::class, [
                'label' => 'Tarif',
                'choices'  => [
                    'Enfant, Etudiant, tarifs réduits : 15 €' => '15',
                    'Adulte : 20 €' => '20',
                    '6 séances : 60 €' => '60',
                ],
            ])
            ->add('acceptance', CheckboxType::class, [
                'label' => 'J\'autorise la Clinique Oscar à utiliser mes coordonnées 
                uniquement pour me contacter et gérer mon rendez-vous.'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
